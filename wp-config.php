<?php
define('WP_CACHE', true); // Added by WP Rocket
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link http://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'smurfitw_db');

/** MySQL database username */
define('DB_USER', 'smurfitw_db');

/** MySQL database password */
define('DB_PASSWORD', 'pRc45eAvZwVtfqy7');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '__ANPR!Ck|&Y=v,J8S7*/gWttz7i|`1R>_D!5a(GNxhD}[(Ht#Jq1|&1_,zO_cyd');
define('SECURE_AUTH_KEY',  '?%F+|m_G`).Mp*@x>L^pn$#|B0;)%2!|g-0*UUBKN6( <SJL&.,$iyJ8C$M!DB #');
define('LOGGED_IN_KEY',    'wj|]czo9W6$PBJ%;_5yK.e*X_61d+F?TK)0UgP?,Yqq89xO8M9o+cCs:L0 PhDE6');
define('NONCE_KEY',        'v6[!~cG%w|`7r.UOJ.yQLt92Qs)lY[yNVoy:#++t]Sv`.t^$)^_7@D)3>uH$c7HH');
define('AUTH_SALT',        '_?P~N|+sS#50leMoCd>9jyx2MEHk<_-gj=VEZA&RfeSoems>uQzP7T+GBrT;d+$]');
define('SECURE_AUTH_SALT', 'yxlMj#q8T+/h1Eb|>}]N,W8N|%H2uH):3Fg|&)n}}+fhT8|SNWjmpPL|U7k?|0l^');
define('LOGGED_IN_SALT',   'WY]<fc>v$$TU+<n.;Qy-5${02BL($=A|$K+=v|}^d9rXNU}IM&b;Aq(2WP|]O4+_');
define('NONCE_SALT',       'U;^.ZxpaW+A_::g+]o/tA99k)8C8Ke-$1Q?e6Q^2f|{BERBfn0- nd{wri|!xzd8');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'sws_';


/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
