<?php
global $errors;

if(!isset($errors)) {
	$errors = "";
}


global $wp_version, $error;

nocache_headers();
header( 'Content-Type: ' . get_bloginfo( 'html_type' ) . '; charset=' . get_bloginfo( 'charset' ) );

?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>
<head>
	<meta http-equiv="Content-Type" content="<?php bloginfo( 'html_type' ); ?>; charset=<?php bloginfo( 'charset' ); ?>" />
	<title>Smurfit Webshop</title>
	<link rel="stylesheet" type="text/css" href="<?php echo OPCO_TEMPLATE_URI . 'style.css' ?>">
</head>
<body>


<div id="opco-login">

	<?php do_action( 'password_protected_login_messages' ); ?>
	<?php do_action( 'password_protected_before_login_form' ); ?>

	<h2 class="error">
		<center>
			<?php echo $errors; ?>
		</center>
	</h2>

	<form name="loginform" id="loginform" action="<?php echo esc_url( home_url( '/' ) ); ?>" method="post">
		<h1>
			<a href="javascript:void(0)" onclick="submitForm()">
				<center>
					<img class="lock-icon" src="<?php echo OPCO_TEMPLATE_URI; ?>lock.png">
				</center>
			</a>
		</h1>

		<p>
			<input placeholder="Vul hier uw opco code in" type="password" name="opco_password" id="opco_password" class="input" value="" size="20" tabindex="20" />
		</p>

		<h2>
			Open de webshop door uw OPCO code in te vullen en op enter te drukken
		</h2>
	</form>

</div>

<script type="text/javascript">

	function submitForm() {
		document.getElementById('loginform').submit();
	}

	try{

		document.getElementById('password_protected_pass').focus();

	}catch(e){

	}

	if(typeof wpOnload=='function') {
		wpOnload();
	}
</script>

<?php do_action( 'login_footer' ); ?>

<div class="clear"></div>

</body>
</html>