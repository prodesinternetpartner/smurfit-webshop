<?php

// Set some vars
define('OPCO_PATH', 			plugin_dir_path(OPCO_FILE));
define('OPCO_BASENAME',			plugin_basename(OPCO_FILE));
define('OPCO_TEMPLATE_PATH',	plugin_dir_path(OPCO_FILE) . 'templates/');
define('OPCO_TEMPLATE_URI',	plugin_dir_url(OPCO_FILE) . 'templates/');

/********* Autoloading classes *********/

/**
 * Load the classes
 *
 * @param 	string $class Class name
 * @return 	void
 */
function OPCO_autoload($class) {

	$classes = array(
		'opco_init'         => OPCO_PATH . 'classes/Init.php',
		'opco_frontendinit' => OPCO_PATH . 'classes/FrontendInit.php',
		'opco_helper'       => OPCO_PATH . 'classes/Helper.php',
	);


	$classname = strtolower($class);

	if (isset($classes[$classname])) {
		require_once($classes[$classname]);
	}
}

register_activation_hook(OPCO_FILE, 'OPCO_activate');


/**
 * This only gets called 1 time
 * Create the opco table
 */
function OPCO_activate() {
	global $wpdb;

	$table_name = $wpdb->prefix . 'opco';


	$sql = "CREATE TABLE {$table_name} (
	  id mediumint(9) NOT NULL AUTO_INCREMENT,
	  opco varchar(255) NOT NULL,
	  user_id mediumint(9) NOT NULL,
	  UNIQUE KEY id (id)
	) {$charset_collate};";

	require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
	dbDelta($sql);


}


register_deactivation_hook(OPCO_FILE, 'OPCO_deactivate');

/**
 * Removes our OPCO table on deactivation
 */
function OPCO_deactivate() {
	global $wpdb;

	$table = $wpdb->prefix . 'opco';

	$wpdb->query("DROP TABLE IF EXISTS {$table}");
}

// Use above function to load the classes automatically
spl_autoload_register('OPCO_autoload');

# Load our helper class
$GLOBALS['OPCO_Helper'] = new OPCO_Helper;

///$GLOBALS['OPCO_Init'] = new OPCO_Init;
