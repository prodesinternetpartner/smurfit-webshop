<?php

/*
Plugin Name: Opco Login
Plugin URI: http://prodes.nl
Description: Login with unique codes
Version: 1.0
Author: Prodes
Text Domain: opco-login
*/

defined( 'ABSPATH' ) or die( "No script kiddies please!" );

# I know it's ugly, but this config needs to be right here!
if(!defined('OPCO_FILE')) {
	define('OPCO_FILE', __FILE__);
}

# Load the plugin
require_once(dirname(__FILE__) . '/init.php');

