<?php

/**
 * Main Initialization class
 */
class OPCO_Init {


	protected $table, $column, $id, $TemplateName, $database, $cookieName;

	/**
	 * Hook into the wordpress actions
	 */
	function __construct() {
		global $wpdb;

		$this->config               = new stdClass;

		# Store the Wordpress database class in a variable
		$this->config->database     = $wpdb;

		# Set the login template
		$this->config->TemplateName = OPCO_TEMPLATE_PATH . 'login.php';

		# Set the opco table
		$this->config->table        = $this->config->database->prefix . 'opco';

		# Set the password column
		$this->config->column       = 'opco';

		# Set the default cookie name
		$this->config->cookieName   = 'OPCO_login';

		$frontend                   = new OPCO_FrontendInit($this->config);
	}




}