<?php



class OPCO_FrontendInit {

	/**
	 * @var $config Store the config
	 */
	protected $config;

	/**
	 * If the attribute can't be fount
	 * try to return it from the config
	 * @param string $value The config Key
	 *
	 * @return mixed
	 */
	public function __get($value) {
		return $this->config->$value;
	}

	/**
	 * Load the frontend for the plugin
	 * @param stdClass $config The config from the init
	 */
	public function __construct(stdClass $config) {

		$this->config = $config;

		if(!isset($_COOKIE[$this->cookieName]) && empty($_COOKIE[$this->cookieName])) {

			if(!is_admin()) {
				# Catch the first redirect to homepage
				add_action('template_redirect', array($this, 'login'), -1);
			}
		}

	}


	/**
	 * Determine if we need to show the login form
	 * or process the login
	 *
	 */
	public function login() {

		if($this->isLoggedIn()) {

			return;

		} else {

			if(!isset($_POST['opco_password'])) {

				# Show the login form
				$this->showLogin();

				# Stop further execution of Wordpress
				die();

			} else {
				# Check if the form is being posted
				if(!empty($_POST['opco_password'])) {
					$this->processLogin();
				}

				# Show the login form
				$this->showLogin();

			}

		}
	}

	/**
	 * Decide if we need to show the login form
	 * @param string $errors The login error to display
	 *
	 * @return void
	 */
	public function showLogin($errorss = null) {

		global $errors;

		$errors = $errorss;

		# Load our login template
		load_template($this->TemplateName);

		# Stop further execution of Wordpress
		exit;
	}

	/**
	 * Process the login request
	 *
	 * @return void
	 */
	protected function processLogin() {

		# Hash the password
		$password = md5($_POST['opco_password']);

		# Search a user with this password
		$user = $this->database->get_results(sprintf("SELECT * FROM {$this->table} WHERE {$this->column} = '%s'", $password), OBJECT);

		if(empty($user)) {

			# No user is found
			$this->showLogin("Foute OPCO code!");
		} else {
			# Only get the first result
			$user = $user[0];

			$this->loginById($user->user_id);
		}

	}


	/**
	 * Check if the user is logged in by this plugin
	 *
	 * @return boolean
	 */
	protected function isLoggedIn() {
		return is_user_logged_in();
	}

	/**
	 * Set auth cookie to new user_id
	 *
	 * @return void
	 */
	protected function loginById($id) {

		# Clear the auth cookie

		wp_clear_auth_cookie();
		wp_set_auth_cookie( $id, false);
		wp_set_current_user( $id );

		return $this->redirectBack();
	}

	/**
	 * Login has succeeded, redirect the user back to the intended URL
	 *
	 * @return void
	 */
	protected function redirectBack() {
		wp_redirect($_SERVER['HTTP_REFERER']); exit;
	}

}