jQuery(function($) {

	$("#woocommerce-pricing-rules-wrap").undelegate('.float_pricing_rule', 'keydown');

	$('#woocommerce-pricing-rules-wrap').delegate('.float_pricing_rule', 'keydown', function (event) {
		// Allow only backspace, delete and tab
		if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 190) {
			// let it happen, don't do anything
		}
		else {
			// Ensure that it is a number and stop the keypress
			if ((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105) && event.keyCode != 188) {
				event.preventDefault();
			}
		}
	});

})