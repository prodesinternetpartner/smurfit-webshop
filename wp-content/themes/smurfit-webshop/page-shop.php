<?php
/*
Template Name: Shop layout
*/

get_header( 'shop' ); ?>

		<div class="row">
			<div class="col-md-3">
				<ul class="categories">
				<?php

				global $wp_query;
				$cat_obj = $wp_query->get_queried_object();

				$args = array(
				  'taxonomy'     => 'product_cat',
				  'orderby'      => 'name',
				  'show_count'   => 0,
				  'pad_counts'   => 0,
				  'hierarchical' => 0,
				  'title_li'     => 'Categorieën',
				  'hide_empty'   => 0
				);

				$categories = get_categories( $args );
				foreach ($categories as $cat) {
				    if($cat->category_parent == 0) {

				        $category_id = $cat->term_id;
				        $active = ($category_id == $cat_obj->term_id) ? ' class="active"' : '';
				        echo '<li><a ' . $active . ' href="'. get_term_link($cat->slug, 'product_cat') .'">'. $cat->name .'</a></li>';

					}
				} ?>
				</ul>
			</div>

			<div class="col-md-6">

				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
					<div id="text-content">
						<?php the_content(); ?>
					</div>
				<?php endwhile; endif; ?>

			</div>

			<div class="col-md-3">
				<?php get_sidebar('shop'); ?>
			</div>
		</div>


<?php get_footer( 'shop' ); ?>
