<?php
/**
 * Single Product title
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product;

$product_category = wp_get_post_terms(get_the_ID(), 'product_cat');
?>


<?php if (get_post_meta($product->id, 'levertijd')): ?>
	<h4 class="stock">Levertijd: <?php echo get_post_meta($product->id, 'levertijd')[0];?></h4>
<?php else: ?>
	<!-- <h4 class="stock">Op aanvraag</h4> -->
<?php endif ?>
