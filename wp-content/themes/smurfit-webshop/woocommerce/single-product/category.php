<?php
/**
 * Single Product title
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
$product_category = wp_get_post_terms(get_the_ID(), 'product_cat');

$category = ($product_category[0]->name) ? $product_category[0]->name : "Geen categorie";

?>
<h4 id="article-name"><?php echo $category; ?></h4>
