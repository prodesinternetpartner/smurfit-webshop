<?php
/**
 * Simple product add to cart
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product;

if ( ! $product->is_purchasable() ) {
	return;
}

if (get_current_user_id() == 24) {
	return;
}

# Get the product tier price rules from the dynamic pricing plugin
$productTierRules = $product->pricing_rules;

# Get the product
$productTierRules = array_pop($productTierRules)['rules'];

# Store the select amounts
$amounts = array();

foreach($productTierRules as $rule) {
	$amounts[] = $rule['from'];
}

?>

<?php
	// $availability      = $product->get_availability();
	$availability = null; // Remove stock from the page
	$availability_html = empty( $availability['availability'] ) ? '' : '<p class="stock ' . esc_attr( $availability['class'] ) . '">' . esc_html( $availability['availability'] ) . '</p>';

	echo apply_filters( 'woocommerce_stock_html', $availability_html, $availability['availability'], $product );
?>

<?php if ( $product->is_in_stock() ) : ?>

	<?php do_action( 'woocommerce_before_add_to_cart_form' ); ?>

	<form class="cart" method="post" enctype='multipart/form-data'>

	<div class="amount-div">
		<?php echo __('Order amount', 'woocommerce') . ':';

		if(!empty($amounts)) {
			woocommerce_quantity_dropdown($amounts);
		} else {

			woocommerce_quantity_input( array(
					'min_value' => apply_filters( 'woocommerce_quantity_input_min', 1, $product ),
					'max_value' => apply_filters( 'woocommerce_quantity_input_max', $product->backorders_allowed() ? '' : $product->get_stock_quantity(), $product )
			));
		}
		?>
	</div>

	<!-- <div class="left logo">Logo: </div> -->

	<!-- <div class="left select-color"> -->

	 	<?php do_action( 'woocommerce_before_add_to_cart_button' ); ?>
	<!-- </div> -->

	<div class="clearfix"></div>
	 	<div class="own-print">
	 		<?php echo __("Own print? Contact us via specials", 'woocommerce'); ?>
	 	</div>

	 	<input type="hidden" name="add-to-cart" value="<?php echo esc_attr( $product->id ); ?>" />

	 	<button type="submit" class="custom_add_to_cart_button alt"><?php echo $product->single_add_to_cart_text(); ?></button>

		<?php do_action( 'woocommerce_after_add_to_cart_button' ); ?>
	</form>

	<?php do_action( 'woocommerce_after_add_to_cart_form' ); ?>

<?php endif; ?>
