<?php
/**
 * Checkout Form
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

wc_print_notices();

do_action( 'woocommerce_before_checkout_form', $checkout );

// If checkout registration is disabled and not logged in, the user cannot checkout
if ( ! $checkout->enable_signup && ! $checkout->enable_guest_checkout && ! is_user_logged_in() ) {
	echo apply_filters( 'woocommerce_checkout_must_be_logged_in_message', __( 'You must be logged in to checkout.', 'woocommerce' ) );
	return;
}

// filter hook for include new pages inside the payment method
$get_checkout_url = apply_filters( 'woocommerce_get_checkout_url', WC()->cart->get_checkout_url() ); ?>

<form name="checkout" method="post" class="checkout woocommerce-checkout" action="<?php echo esc_url( $get_checkout_url ); ?>" enctype="multipart/form-data">

	<div id="order_review" class="woocommerce-checkout-review-order">
		<strong class="pagetitle">Winkelwagen</strong>
		<p class="cart-has">
			Uw winkelwagen bevat de volgende items:
			<img class="checkout-cart" src="<?php echo get_stylesheet_directory_uri(); ?>/library/images/icon-cart.png" alt="">
		</p>
		<?php do_action( 'woocommerce_checkout_order_review' ); ?>
	</div>

	<?php if ( sizeof( $checkout->checkout_fields ) > 0 ) : ?>

		<?php do_action( 'woocommerce_checkout_before_customer_details' ); ?>

		<div class="col2-set" id="customer_details">
			<div class="col">
				<?php do_action( 'woocommerce_checkout_billing' ); ?>
			</div>
		</div>
		<div class="col2-set">
			<div class="col-1">
				<?php do_action( 'woocommerce_checkout_shipping' ); ?>
			</div>
		</div>

		<?php do_action( 'woocommerce_checkout_after_customer_details' ); ?>


	<?php endif; ?>

	<?php do_action( 'woocommerce_checkout_before_order_review' ); ?>



	<?php do_action( 'woocommerce_checkout_after_order_review' ); ?>

	<?php
		# First remove the order review hook, otherwise we would get the order review again
		remove_action('woocommerce_checkout_order_review', 'woocommerce_order_review');

		# Add the checkout template hook again
		add_action( 'woocommerce_checkout_order_review', 'woocommerce_checkout_payment', 20 );

		# Execute the hook
		do_action('woocommerce_checkout_order_review');
 	?>

</form>

<?php do_action( 'woocommerce_after_checkout_form', $checkout ); ?>
