<?php
/**
 * Checkout shipping information form
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.2.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $current_user;

# Array with all the shipping fields
$shippingFields = array(
	'shipping_company',
	'shipping_first_name',
	'shipping_last_name',
	'shipping_address_1',
	'shipping_address_2',
	'shipping_postcode',
	'shipping_city',
	'shipping_country',
	'shipping_phone',
);
?>


<div class="woocommerce-shipping-fields">

	<strong>Aflever adres:</strong> (<a href="<?php echo get_home_url() ?>/my-account/edit-address/verzending/">Bewerken</a>)<br>

	<?php if ( WC()->cart->needs_shipping_address() === true ) : ?>

		<?php
			if ( empty( $_POST ) ) {

				$ship_to_different_address = get_option( 'woocommerce_ship_to_destination' ) === 'shipping' ? 1 : 0;
				$ship_to_different_address = apply_filters( 'woocommerce_ship_to_different_address_checked', $ship_to_different_address );

			} else {

				$ship_to_different_address = $checkout->get_value( 'ship_to_different_address' );

			}
		?>


		<div class="shipping_address">

			<?php do_action( 'woocommerce_before_checkout_shipping_form', $checkout ); ?>

				<?php

					echo '<input type="hidden" name="ship_to_different_address" value="1">';
					echo '<input type="hidden" name="payment_method" value="cheque">';

					foreach($shippingFields as $field):

						$meta = get_user_meta( $current_user->ID, $field, true );

						if(empty($meta)) {
							continue;
						}

						echo $meta;
						echo '<input type="hidden" name="' . $field . '" id="' . $field . '" value="' . $meta . '">';

						if($field == 'shipping_first_name' || $field == 'shipping_postcode') {
							echo ' ';
						} else {
							echo '<br>';
						}

			 		endforeach;

			 	 ?>

			<?php do_action( 'woocommerce_after_checkout_shipping_form', $checkout ); ?>

		</div>

	<?php endif; ?>
	<br>
	<?php do_action( 'woocommerce_before_order_notes', $checkout ); ?>

	<?php if ( apply_filters( 'woocommerce_enable_order_notes_field', get_option( 'woocommerce_enable_order_comments', 'yes' ) === 'yes' ) ) : ?>

		<?php if ( ! WC()->cart->needs_shipping() || WC()->cart->ship_to_billing_address_only() ) : ?>

			<h3><?php _e( 'Additional Information', 'woocommerce' ); ?></h3>

		<?php endif; ?>

		<?php foreach ( $checkout->checkout_fields['order'] as $key => $field ) : ?>

			<?php woocommerce_form_field( $key, $field, $checkout->get_value( $key ) ); ?>

		<?php endforeach; ?>

	<?php endif; ?>

	<?php do_action( 'woocommerce_after_order_notes', $checkout ); ?>
</div>
