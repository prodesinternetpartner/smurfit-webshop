<?php

/**
 * Review order table
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.3.0
 */

if (!defined('ABSPATH')) {
	exit;
}


?>

<table class="shop_table woocommerce-checkout-review-order-table">
	<tbody>
		<?php
			do_action( 'woocommerce_review_order_before_cart_contents' );

			foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
				$_product     = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
				if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_checkout_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
					?>
					<tr class="<?php echo esc_attr( apply_filters( 'woocommerce_cart_item_class', 'cart_item', $cart_item, $cart_item_key ) ); ?>">
						<td class="product-remove">
							<?php
								echo apply_filters( 'woocommerce_cart_item_remove_link', sprintf( '<a href="%s" class="remove" title="%s">&times;</a>', esc_url( WC()->cart->get_remove_url( $cart_item_key ) ), __( 'Remove this item', 'woocommerce' ) ), $cart_item_key );
							?>
						</td>
						<td class="product-name">
							<?php echo apply_filters( 'woocommerce_cart_item_name', $_product->get_title(), $cart_item, $cart_item_key ); ?>
						</td>
						<td class="product-quantity">
							<?php
							$amount = ($cart_item['quantity'] > 1) ? "stuks" : "stuk";
							echo $cart_item['quantity'] . ' ' . $amount; ?>
						</td>
						<td class="product-meta">
							<?php echo WC()->cart->get_item_data( $cart_item ); ?>
						</td>
						<td class="product-quantity">
							<?php
								if ( $_product->is_sold_individually() ) {
									$product_quantity = sprintf( '1 <input type="hidden" name="cart[%s][qty]" value="1" />', $cart_item_key );
								} else {
									$product_quantity = woocommerce_quantity_input( array(
										'input_name'  => "cart[{$cart_item_key}][qty]",
										'input_value' => $cart_item['quantity'],
										'max_value'   => $_product->backorders_allowed() ? '' : $_product->get_stock_quantity(),
										'min_value'   => '0'
									), $_product, false );
								}
								// dd(WC()->cart->get_product_price( $_product ));
								// Echo out the amount x price, and remove the euro sign
								echo $cart_item['quantity'] . ' x ' . WC()->cart->get_product_price( $_product );
							?>
						</td>
						<td class="product-total">
							<?php echo apply_filters( 'woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal( $_product, $cart_item['quantity'] ), $cart_item, $cart_item_key ); ?>
						</td>
					</tr>
					<?php
				}
			}
?>
			<tr>
				<td colspan="4"><strong class="total-text">Totaal</strong></td>
				<td><strong class="total-amount"><?php echo WC()->cart->get_cart_subtotal();?></strong></td>
			</tr>
<?php
			do_action( 'woocommerce_review_order_after_cart_contents' );
		?>
	</tbody>

</table>
