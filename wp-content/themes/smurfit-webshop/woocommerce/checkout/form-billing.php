<?php
/**
 * Checkout billing information form
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.1.2
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
global $current_user;
/** @global WC_Checkout $checkout */

# Array with all the billing fields
$billingFields = array(
	'billing_company',
	'billing_first_name',
	'billing_last_name',
	'billing_address_1',
	'billing_address_2',
	'billing_postcode',
	'billing_city',
	'billing_country',
	'billing_phone',
);

?>
<div class="woocommerce-billing-fields">
	<?php if ( WC()->cart->ship_to_billing_address_only() && WC()->cart->needs_shipping() ) : ?>

		<?php echo "<strong>Besteld door (factuur naar):<br></strong>"; ?>

	<?php else : ?>

		<?php echo "<strong>Besteld door (factuur naar):<br></strong>"; ?>

	<?php endif; ?>

	<?php do_action( 'woocommerce_before_checkout_billing_form', $checkout ); ?>

		<?php
			foreach($billingFields as $field):

				$meta = get_user_meta( $current_user->ID, $field, true );
				// d(get_user_meta( $current_user->ID));
				if(empty($meta)) {
					continue;
				}

				echo $meta;
				echo '<input type="hidden" name="' . $field . '" id="' . $field . '" value="' . $meta . '">';

				if($field == 'billing_first_name' || $field == 'billing_postcode' ||  $field == 'billing_phone' || $field == 'billing_country') {
					echo ' ';
				} else {
					echo '<br>';
				}

	 		endforeach;
	 	?>


		<div id="checkout-po-number">
			<?php

				woocommerce_form_field( 'po_number', array(
					'type' 			=> 'text',
					'class' 		=> array(''),
					'placeholder' 	=> __(''),
					), $checkout->get_value( 'po_number' ));

			?>
			<span>PO nummer (verplicht)</span>
		</div>

	 	 <br>
	 	 <br>


	<?php do_action('woocommerce_after_checkout_billing_form', $checkout ); ?>
	<?php if ( ! is_user_logged_in() && $checkout->enable_signup ) : ?>

		<?php if ( $checkout->enable_guest_checkout ) : ?>

			<p class="form-row form-row-wide create-account">
				<input class="input-checkbox" id="createaccount" <?php checked( ( true === $checkout->get_value( 'createaccount' ) || ( true === apply_filters( 'woocommerce_create_account_default_checked', false ) ) ), true) ?> type="checkbox" name="createaccount" value="1" /> <label for="createaccount" class="checkbox"><?php _e( 'Create an account?', 'woocommerce' ); ?></label>
			</p>

		<?php endif; ?>

		<?php do_action( 'woocommerce_before_checkout_registration_form', $checkout ); ?>

		<?php if ( ! empty( $checkout->checkout_fields['account'] ) ) : ?>

			<div class="create-account">

				<p><?php _e( 'Create an account by entering the information below. If you are a returning customer please login at the top of the page.', 'woocommerce' ); ?></p>

				<?php foreach ( $checkout->checkout_fields['account'] as $key => $field ) : ?>

					<?php woocommerce_form_field( $key, $field, $checkout->get_value( $key ) ); ?>

				<?php endforeach; ?>

				<div class="clear"></div>

			</div>

		<?php endif; ?>

		<?php do_action( 'woocommerce_after_checkout_registration_form', $checkout ); ?>

	<?php endif; ?>
</div>
