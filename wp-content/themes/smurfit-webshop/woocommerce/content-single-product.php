<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * Override this template by copying it to yourtheme/woocommerce/content-single-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>

<?php
	/**
	 * woocommerce_before_single_product hook
	 *
	 * @hooked wc_print_notices - 10
	 */
	 do_action( 'woocommerce_before_single_product' );

	 if ( post_password_required() ) {
	 	echo get_the_password_form();
	 	return;
	 }
?>

<div class="row">
	<div class="col-md-3">
		<ul class="categories">
		<?php

		global $wp_query;
		$cat_obj = $wp_query->get_queried_object();

		$args = array(
		  'taxonomy'     => 'product_cat',
		  'orderby'      => 'name',
		  'show_count'   => 0,
		  'pad_counts'   => 0,
		  'hierarchical' => 0,
		  'title_li'     => 'Categorieën',
		  'hide_empty'   => 0
		);

		$categories = get_categories( $args );

		$product_category_id = wp_get_post_terms(get_the_ID(), 'product_cat');

		foreach ($categories as $cat) {
		    if($cat->category_parent == 0) {

		        $category_id = $cat->term_id;
		        $active = ($category_id == $product_category_id[0]->term_id) ? ' class="active"' : '';
		        echo '<li><a ' . $active . ' href="'. get_term_link($cat->slug, 'product_cat') .'">'. $cat->name .'</a></li>';

			}
		} ?>
		</ul>
	</div>

	<div class="col-md-6">

		<div itemscope itemtype="<?php echo woocommerce_get_product_schema(); ?>" id="product-<?php the_ID(); ?>" <?php post_class(); ?>>

			<?php
				/**
				 * woocommerce_before_single_product_summary hook
				 *
				 * @hooked woocommerce_show_product_sale_flash - 10
				 * @hooked woocommerce_show_product_images - 20
				 */
				do_action( 'woocommerce_before_single_product_summary' );
			?>

			<div class="summary entry-summary">

				<?php
					/**
					 * woocommerce_single_product_summary hook
					 *
					 * @hooked woocommerce_template_single_title - 5
					 * @hooked woocommerce_template_single_rating - 10
					 * @hooked woocommerce_template_single_price - 10
					 * @hooked woocommerce_template_single_excerpt - 20
					 * @hooked woocommerce_template_single_add_to_cart - 30
					 * @hooked woocommerce_template_single_meta - 40
					 * @hooked woocommerce_template_single_sharing - 50
					 */
					do_action( 'woocommerce_single_product_summary' );
				?>

			</div><!-- .summary -->

			<div class="woocommerce-tabs">
				<?php wc_get_template( 'single-product/tabs/description.php'); ?>
			</div>

			<div>
				<?php add_action('woocommerce_template_single_add_to_cart', 'woocommerce_template_single_add_to_cart') ?>
				<?php do_action('woocommerce_template_single_add_to_cart'); ?>
			</div>


			<meta itemprop="url" content="<?php the_permalink(); ?>" />

		</div><!-- #product-<?php the_ID(); ?> -->
	</div>

	<div class="col-md-3">
		<?php get_sidebar('shop'); ?>
	</div>

</div>

<?php do_action( 'woocommerce_after_single_product' ); ?>
