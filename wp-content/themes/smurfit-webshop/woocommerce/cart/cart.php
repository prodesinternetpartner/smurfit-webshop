<?php
/**
 * Cart Page
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

wc_print_notices();

do_action( 'woocommerce_before_cart' ); ?>

<form action="<?php echo esc_url( WC()->cart->get_cart_url() ); ?>" method="post">
	<div id="custom_shopping_cart">

		<strong class="pagetitle"><?php echo __('Shoppingcart', 'smurfit-webshop'); ?></strong>


		<?php do_action( 'woocommerce_before_cart_table' ); ?>

		<table class="shop_table cart" cellspacing="0">
			<thead>
				<tr>
					<th class="product-thumbnail">&nbsp;</th>
					<th class="product-name">&nbsp;</th>
					<th class="product-price">&nbsp;</th>
					<th class="product-quantity">&nbsp;</th>
					<th class="product-quantity">&nbsp;</th>
					<th class="product-subtotal">&nbsp;</th>
				</tr>
			</thead>
			<tbody>
				<?php do_action( 'woocommerce_before_cart_contents' ); ?>

				<?php
				foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
					$_product     = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
					$product_id   = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );

					if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
						?>
						<tr class="<?php echo esc_attr( apply_filters( 'woocommerce_cart_item_class', 'cart_item', $cart_item, $cart_item_key ) ); ?>">


							<td class="product-name">
								<?php
									if ( ! $_product->is_visible() ) {
										echo apply_filters( 'woocommerce_cart_item_name', $_product->get_title(), $cart_item, $cart_item_key );
									}
									else {
										echo apply_filters( 'woocommerce_cart_item_name', sprintf( '<a class="custom_product_link" href="%s">%s</a>', $_product->get_permalink( $cart_item ), $_product->get_title() ), $cart_item, $cart_item_key );
									}

		               				// Backorder notification
		               				if ( $_product->backorders_require_notification() && $_product->is_on_backorder( $cart_item['quantity'] ) )
		               					echo '<p class="backorder_notification">' . __( 'Available on backorder', 'woocommerce' ) . '</p>';
								?>
							</td>

							<td class="product-price">
								<?php
									// Set the amount
									$amount = $cart_item['quantity'];
									$amount .= ($cart_item['quantity'] > 1) ? ' stuks' : ' stuk';
									echo $amount;
								?>
							</td>

							<td>
								<?php

									// Meta data
									echo WC()->cart->get_item_data( $cart_item );
								?>
							</td>
							<td class="product-quantity">
								<?php
									if ( $_product->is_sold_individually() ) {
										$product_quantity = sprintf( '1 <input type="hidden" name="cart[%s][qty]" value="1" />', $cart_item_key );
									} else {
										$product_quantity = woocommerce_quantity_input( array(
											'input_name'  => "cart[{$cart_item_key}][qty]",
											'input_value' => $cart_item['quantity'],
											'max_value'   => $_product->backorders_allowed() ? '' : $_product->get_stock_quantity(),
											'min_value'   => '0'
										), $_product, false );
									}

									// Echo out the amount x price, and remove the euro sign
									echo $cart_item['quantity'] . ' x' . WC()->cart->get_product_price( $_product );
								?>
							</td>

							<td class="product-subtotal">
								<?php
									// Echo out the product subtotal and remove the euro sign
									echo  apply_filters( 'woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal( $_product, $cart_item['quantity'] ), $cart_item, $cart_item_key );
								?>
							</td>
						</tr>
						<?php
					}
				}

				do_action( 'woocommerce_cart_contents' );
				?>
				<?php /* Removed coupons etc. here */ ?>

				<?php do_action( 'woocommerce_after_cart_contents' ); ?>
			</tbody>
		</table>

		<?php do_action( 'woocommerce_after_cart_table' ); ?>




	</div>

	<div class="cart-collaterals">
		<?php do_action( 'woocommerce_cart_collaterals' ); ?>
		<?php woocommerce_cart_totals(); ?>

	</div>

	<div class="order-data">
		<?php // Get billing data
			$data = getAddress();

			echo "Besteld door: (factuur naar)" . ': <br>';
			echo $data['_billing_company'][0] . "<br>";
			echo $data['_billing_first_name'][0] . ' ' . $data['_billing_last_name'][0] . "<br>";
			echo $data['_billing_address_1'][0] . "<br>";
			echo $data['_billing_postcode'][0] . ' ' . $data['_billing_city'][0] . "<br>";
			echo $data['_billing_country'][0] . "<br>";
			echo $data['_billing_phone'][0];
		?>
		<div class="po_number">

			<?php

			echo __('PO number (mandatory)', 'smurfit-webshop') . ': ';
			woocommerce_form_field('_billing_po_number', array(
				'type' => 'text',
				'class' => 'po_number_input',
			));

			?>
		</div>

		<br><br>

		<?php // Show the delivery data
			echo __('Deliveryaddress', 'smurfit-webshop') . ': <br>';
			echo $data['_shipping_company'][0] . "<br>";
			echo $data['_shipping_first_name'][0] . '<br>';
			echo $data['_shipping_address_1'][0] . "<br>";
			echo $data['_shipping_postcode'][0] . '<br>';
			echo $data['_shipping_phone'][0];
		?>

		<br><br>

		<?php echo __('Notes') . ': ' . $data['order_comments']; ?>

		<?php woocommerce_button_proceed_to_checkout('small-button', 'Plaats bestelling') ?>

	</div>

</form>
<?php do_action( 'woocommerce_after_cart' ); ?>
