<?php
/**
 * Mini-cart
 *
 * Contains the markup for the mini-cart, used by the cart widget
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
?>

<?php do_action( 'woocommerce_before_mini_cart' ); ?>

<ul class="cart_list product_list_widget <?php echo $args['list_class']; ?>">

	<?php if ( sizeof( WC()->cart->get_cart() ) > 0 ) : ?>
		<a href="<?php echo redirect_to_checkout(); ?>">

			<div class="col-md-3">
				<img src="<?php bloginfo('stylesheet_directory'); ?>/library/images/icon-cart.png">
			</div>

			<div class="col-md-9">
				Uw winkelwagen bevat<br>
				<?php echo WC()->cart->get_cart_contents_count(); ?> item(s)
			</div>

			<div class="clearfix"></div>

		</a>

	<?php else : ?>

		<li class="empty"><?php _e( 'No products in the cart.', 'woocommerce' ); ?></li>

	<?php endif; ?>

</ul><!-- end product list -->

<?php do_action( 'woocommerce_after_mini_cart' ); ?>
